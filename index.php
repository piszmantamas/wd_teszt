<?php

class warehouse 
{

	private $wh_u_id=""; //warehouse unique id (expansion)
	private $name="";
	private $adress="";
	private $sum_capacity="";	
	private $prod_list = array("unique_id" => array(), "count" => array());

	public function __get($property) 
	{
		if (property_exists($this, $property)) 
		{
      		return $this->$property;
    	}
	}

	public function __set($property, $value) 
	{
	    if (property_exists($this, $property)) 
	    {
	    	$this->$property = $value;
	    }
    	return $this;
  	}

	public function __toString() 
	{
        $ret_val = "Unique id: " . $this->wh_u_id . "<br>Name: " . $this->name  . "<br>Adress: " .  $this->adress  . "<br>Capatitate: " .  $this->sum_capacity;
        return $ret_val;
    }

	public function add_item($u_id,$amount) 
	{
		$search_val = array_search($u_id, $this->prod_list['unique_id']);
		if ( $this->sum_capacity < $amount || (array_sum($this->prod_list['count']) + $amount) > $this->sum_capacity) {
			if ( $search_val !== false ) {
				throw new Exception ("The warehouse capacity is not enought for expand exists product.",31);
			} else {
				throw new Exception ("The warehouse capacity is not enought for add new product.",30);
			}
		} else {
			if ( $search_val !== false ) {
				$this->prod_list['count'][$search_val] += $amount;
			} else {
				array_push($this->prod_list['count'], $amount);
				array_push($this->prod_list['unique_id'], $u_id);
			}
		}
	}

	public function take_out_item($u_id,$amount) 
	{
		$search_val = array_search($u_id, $this->prod_list['unique_id']);
		if ( $search_val !== false ) {
			if ( $this->prod_list['count'][$search_val] === $amount) {
				unset($this->prod_list['count'][$search_val]);
				unset($this->prod_list['unique_id'][$search_val]);
			} elseif ($this->prod_list['count'][$search_val] >= $amount) {
				$this->prod_list['count'][$search_val] -= $amount;
			} else {
				throw new Exception ("Not enought item in this warehouse.",40);
			}
		} else {
			throw new Exception ("This item cannot be found in this warehouse.",41);
		}
	}

}

class brand 
{
	private $brand_id="";
	private $name="";
	private $quality=""; // 1-5
	private $type=""; // expansion

	public function __get($property) 
	{
		if (property_exists($this, $property)) 
		{
      		return $this->$property;
    	}
	}

	public function __set($property, $value) 
	{
	    if (property_exists($this, $property)) 
	    {
	    	$this->$property = $value;
	    }
    	return $this;
  	}

  	public function __toString() 
  	{
        $ret_val = "Brand id: " . $this->brand_id . "<br>Name: " . $this->name  . "<br>Quality: " .  $this->quality;
        return $ret_val;
    }
}

class product 
{

	private $item_nr=""; //unique item number
	private $name="";
	private $price="";
	private $prod_brand_id="";
	private $type=""; // expansion

	public function __get($property) 
	{
		if (property_exists($this, $property)) 
		{
      		return $this->$property;
    	}
	}

	public function __set($property, $value) 
	{
	    if (property_exists($this, $property)) 
	    {
	    	$this->$property = $value;
	    }
    	return $this;
  	}

  	public function __toString() 
  	{
        $ret_val = "Item number: " . $this->item_nr . "<br>Name: " . $this->name  . "<br>Price: " .  $this->price;
        return $ret_val;
    }

}

#Two product class completed
$termek1 = new product();
$termek1->name = "hover";
$termek1->item_nr = 123456;
$termek1->price = 1500;
$termek1->prod_brand_id = 1;
$termek1->type = "machine";

$termek2 = new product();
$termek2->name = "cooker";
$termek2->item_nr = 654321;
$termek2->price = 2500;
$termek2->prod_brand_id = 1;
$termek2->type = "machine";

$marka1 = new brand();
$marka1->name = "marka1";
$marka1->brand_id = 1;
$marka1->quality = 5;
$marka1->type = "machine";

/*echo "<h4>" . $marka1 . "</h4>";
echo "<h4>" . $termek1 . "</h4>";
echo "<h4>" . $termek2 . "</h4>";*/

#Create warehouses
$raktar1 = new warehouse();
$raktar1->wh_u_id = 1;
$raktar1->name = "raktar1";
$raktar1->adress = "cim1";
$raktar1->sum_capacity = 20;

$raktar2 = new warehouse();
$raktar2->wh_u_id = 2;
$raktar2->name = "raktar2";
$raktar2->adress = "cim2";
$raktar2->sum_capacity = 50;

#Instance 1
#Add items warehouse
try {
	$raktar1->add_item(123456,10);
} catch (Exception $ex) {
	$raktar2->add_item(123456,10);
}

try {	
	$raktar1->add_item(654321,5);
} catch (Exception $ex) {
	//echo $ex;
	$raktar2->add_item(654321,5);
}

#Take out items from warehouse
try {
	$raktar1->take_out_item(123456,8);
} catch (Exception $ex) {
	echo  "[" . $ex->getCode() . "] " .$ex;
}

#Show items in warehouse
echo "<table border=1><tr>";
echo "<td colspan=2>Case 1: Create WH1, WH2 and add x prod and take out y.</td>";

echo "<tr>";
echo "<td><h4>" . $raktar1 ."</h4></td>";
echo "<td><pre>";
print_r($raktar1->prod_list);
echo "</pre></td>";
echo "</tr>";

echo "<tr>";
echo "<td><h4>" . $raktar2 ."</h4></td>";
echo "<td><pre>";
print_r($raktar2->prod_list);
echo "</pre></td>";
echo "</tr>";
echo "</table><br>";

echo "<table border=1><tr>";
echo "<td colspan=2>Case 2: Create WH1, WH2 and add x prod to WH1, but in WH1 not enought place.</td>";
#Instance 2
#Add items warehouse
try {
	$raktar1->add_item(123456,15);
} catch (Exception $ex) {
	switch ($ex->getCode()) {
		case 30: //Not enought place in the first warehouse -> Stock up the second (new product)
			echo  "[" . $ex->getCode() . "] " .$ex;
			$raktar2->add_item(123456,15);
			break;
		
		case 31: //Stock up the first warehouse and if need the second (exists product)
			$contains = array_sum($raktar1->prod_list['count']);
			$val = $raktar1->sum_capacity-$contains;
			$raktar1->add_item(123456,$val);
			$raktar2->add_item(123456,15-$val);
			break;

		case 40: //more than if it contains
			echo  "[" . $ex->getCode() . "] " .$ex;
			break;

		case 41: //no exists item
			echo  "[" . $ex->getCode() . "] " .$ex;
			break;
	}
}

echo "<tr>";
echo "<td><h4>" . $raktar1 ."</h4></td>";
echo "<td><pre>";
print_r($raktar1->prod_list);
echo "</pre></td>";
echo "</tr>";

echo "<tr>";
echo "<td><h4>" . $raktar2 ."</h4></td>";
echo "<td><pre>";
print_r($raktar2->prod_list);
echo "</pre></td>";
echo "</tr>";
echo "</table><br>";


echo "<table border=1><tr>";
echo "<td colspan=2>Case 3: Create WH1, WH2 and add x prod and take out more than enought prod.</td></tr>";
#Instance 3
#Take out items from warehouse
try {
	$raktar1->take_out_item(123456,100);
} catch (Exception $ex) {
	echo  "[" . $ex->getCode() . "] " .$ex;
}

echo "<tr>";
echo "<td><h4>" . $raktar1 ."</h4></td>";
echo "<td><pre>";
print_r($raktar1->prod_list);
echo "</pre></td>";
echo "</tr>";

echo "<tr>";
echo "<td><h4>" . $raktar2 ."</h4></td>";
echo "<td><pre>";
print_r($raktar2->prod_list);
echo "</pre></td>";
echo "</tr>";
echo "</table>";